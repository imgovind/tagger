package tagPackage;

import java.util.ArrayList;
import java.util.List;

public class RegularEx {
	
	public static base base = new base();
	
	public List<String[]> RemoveSpecialCharacters(List<String[]> inputList)
	{
		List<String[]> outputList = null;
		RegularEx thisClass = new RegularEx();
		if(inputList != null && !inputList.isEmpty())
		{
			outputList = new ArrayList<String[]>();
			for(String[] row: inputList)
			{
				if(row != null)
				{
					if(!row[0].equals("0"))
					{
						row[1] = thisClass.ProcessSpecialCharacters(row[1]);
						row[2] = thisClass.ProcessSpecialCharacters(row[2]);
					}
				}
				outputList.add(row);
			}
		}
		return outputList;
	}	
	public String ProcessSpecialCharacters(String inputString)
	{		
		String outputString = base.defaultString();
		try
		{
			outputString = inputString.replaceAll("\\<.*?>","").replaceAll("[-_+!^*(&)%{$}@[#]=|;\\:<.>\"/?,~`]", " ").replaceAll(" $","").replaceAll("\\s+", " ");
		}
		catch(Exception ex)
		{
			base.printElementln(ex);
		}
		return outputString;
	}	
}
