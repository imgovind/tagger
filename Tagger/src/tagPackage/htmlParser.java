package tagPackage;

import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;

public class htmlParser {
	
	public static base base = new base();
	
	public String removeTags(String inputString, List<String> tags)
	{
		String outputString = base.defaultString();
		Document doc = Jsoup.parse(inputString);
		for(String tag: tags)
		{
			doc.select(tag).remove();
		}
		StringBuilder sb = new StringBuilder();
		for(Element element: doc.select("p"))
		{
			sb.append(element.text());
		}
		outputString = sb.toString();
		return outputString;
	}
}
