package tagPackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Contractions {

	public static base base = new base();
	
	public static Map<String,String> listOfContractions = new HashMap<String,String>();
	
	public Contractions()
	{
		//I
		//listOfContractions.put("I'm", "I am");
		listOfContractions.put("i'll", "I will");
		//listOfContractions.put("I'd", "I would");
		listOfContractions.put("i've", "I have");
		//listOfContractions.put("I'd", "I had");
		//You
		//listOfContractions.put("you're", "you are");
		listOfContractions.put("you'll", "you will");
		//listOfContractions.put("you'd", "you would");
		listOfContractions.put("you've", "you have");
		//listOfContractions.put("you'd", "you had");
		//he
		//listOfContractions.put("he's", "he is");
		listOfContractions.put("he'll", "he will");
		//listOfContractions.put("he'd", "he would");
		//listOfContractions.put("he's", "he has");
		//listOfContractions.put("he'd", "he had");
		//she
		//listOfContractions.put("she's", "she is");
		listOfContractions.put("she'll", "she will");
		//listOfContractions.put("she'd", "she would");
		//listOfContractions.put("she's", "she has");
		//listOfContractions.put("she'd", "she had");
		//it
		//listOfContractions.put("it's", "it is");
		listOfContractions.put("it'll", "it will");
		//listOfContractions.put("it'd", "it would");
		//listOfContractions.put("it's", "it has");
		//listOfContractions.put("it'd", "it had");
		//we
		listOfContractions.put("we're", "we are");
		listOfContractions.put("we'll", "we will");
		//listOfContractions.put("we'd", "we would");
		listOfContractions.put("we've", "we have");
		//listOfContractions.put("we'd", "we had");
		//they
		listOfContractions.put("they're", "they are");
		listOfContractions.put("they'll", "they will");
		listOfContractions.put("they'd", "they would");
		listOfContractions.put("they've", "they have");
		listOfContractions.put("they'd", "they had");
		//that
		//listOfContractions.put("that's", "that is");
		listOfContractions.put("that'll", "that will");
		//listOfContractions.put("that'd", "that would");
		//listOfContractions.put("that's", "that has");
		//listOfContractions.put("that'd", "that had");
		//who
		//listOfContractions.put("who's", "who is");
		listOfContractions.put("who'll", "who will");
		//listOfContractions.put("who'd", "who would");
		//listOfContractions.put("who's", "who has");
		//listOfContractions.put("who'd", "who had");
		//what
		//listOfContractions.put("what's", "what is");
		listOfContractions.put("what're", "what are");
		listOfContractions.put("what'll", "what will");
		//listOfContractions.put("what'd", "what would");
		//listOfContractions.put("what's", "what has");
		//listOfContractions.put("what'd", "what had");
		//where
		//listOfContractions.put("where's", "where is");
		listOfContractions.put("where'll", "where will");
		//listOfContractions.put("where'd", "where would");
		//listOfContractions.put("where's", "where has");
		//listOfContractions.put("where'd", "where had");
		//when
		//listOfContractions.put("when's", "when is");
		listOfContractions.put("when'll", "when will");
		//listOfContractions.put("when'd", "when would");
		//listOfContractions.put("when's", "when has");
		//listOfContractions.put("when'd", "when had");
		//why
		//listOfContractions.put("why's", "why is");
		listOfContractions.put("why'll", "why will");
		//listOfContractions.put("why'd", "why would");
		//listOfContractions.put("why's", "why has");
		//listOfContractions.put("why'd", "why had");
		//how
		//listOfContractions.put("how's", "how is");
		listOfContractions.put("how'll", "how will");
		//listOfContractions.put("how'd", "how would");
		//listOfContractions.put("how's", "how has");
		//listOfContractions.put("how'd", "how had");
		//Other Negative Contractions
		listOfContractions.put("isn't", "is not");
		listOfContractions.put("aren't", "are not");
		listOfContractions.put("wasn't", "was not");
		listOfContractions.put("weren't", "were not");
		listOfContractions.put("haven't", "have not");
		listOfContractions.put("hasn't", "has not");
		listOfContractions.put("hadn't", "had not");
		listOfContractions.put("won't", "will not");
		listOfContractions.put("wouldn't", "would not");
		listOfContractions.put("don't", "do not");
		listOfContractions.put("doesn't", "does not");
		listOfContractions.put("didn't", "did not");
		listOfContractions.put("can't", "cannot");
		listOfContractions.put("couldn't", "could not");
		listOfContractions.put("shouldn't", "should not");
		listOfContractions.put("mightn't", "might not");
		listOfContractions.put("ain't", "am not");
		listOfContractions.put("mayn't", "may not");
		listOfContractions.put("mustn't", "must not");
		listOfContractions.put("needn't", "need not");
		listOfContractions.put("shan't", "shall not");
		//woulda-shoulda-coulda
		listOfContractions.put("would've", "would have");
		listOfContractions.put("should've", "should have");
		listOfContractions.put("could've", "could have");
		listOfContractions.put("might've", "might have");
		listOfContractions.put("must've", "must have");
	}
	
	public List<String[]> RemoveContractions(List<String[]> inputList)
	{
		List<String[]> outputList = null;
		Contractions thisClass = new Contractions();
		if(inputList != null && !inputList.isEmpty())
		{
			outputList = new ArrayList<String[]>();
			for(String[] row: inputList)
			{
				if(row != null)
				{
					if(!row[0].equals("0"))
					{
						row[1] = thisClass.ProcessContractions(row[1]);
						row[2] = thisClass.ProcessContractions(row[2]);
					}
				}
				outputList.add(row);
			}
		}
		return outputList;
	}
	
	public String ProcessContractions(String inputString)
	{
		String outputString = base.defaultString();
		if(!inputString.isEmpty())
		{
			/*
			Iterator<Entry<String, String>> iter = listOfContractions.entrySet().iterator();
			
			while(iter.hasNext())
			{
				Map.Entry<String, String> kvp = (Map.Entry<String, String>)iter.next();
				if(inputString.contains(kvp.getKey()))
				{
					outputString = inputString.toLowerCase().replace(kvp.getKey(), kvp.getValue());
				}
				else
				{
					outputString = inputString;
				}
				outputString = outputString.trim();
			}
			*/
			
			String[] inputStringWords = inputString.split(" ");
			StringBuilder sb = new StringBuilder();
			for(String string: inputStringWords)
			{
				if(listOfContractions.containsKey(string.toLowerCase()))
				{
					string = listOfContractions.get(string);
				}
				sb.append(string);
				sb.append(" ");
			}
			outputString = sb.toString().trim();
		}
		return outputString;
	}
}
