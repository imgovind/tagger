package tagPackage;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class FileParse {
	
	public static base base = new base();
	public static htmlParser htmlParser = new htmlParser();
	
	public List<String[]> parseAllFile(String inputFile)
	{
		try
		{
			CSVReader csvReader = new CSVReader(new FileReader(inputFile));
			List<String[]> allContent = null;
			List<String> tags = new ArrayList<String>();
			tags.add("pre");
			tags.add("code");
			String row[] = null;
			row =csvReader.readNext();
			if(row != null)
			{
				allContent = new ArrayList<String[]>();
				allContent = csvReader.readAll();
			}
			csvReader.close();
			for(String[] rowInput: allContent)
			{
				if(!rowInput[0].equals("Id"))
				{
					rowInput[2] = htmlParser.removeTags(rowInput[2], tags);
				}
			}
			return allContent;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			return null;
		}
	}
	
	public List<String[]> parseFile(String inputFile)
	{
		try
		{
			CSVReader csvReader = new CSVReader(new FileReader(inputFile));
			List<String[]> allContent = null;
			String row[] = null;
			row = csvReader.readNext();
			if(row != null)
			{
				allContent = new ArrayList<String[]>();
				allContent = csvReader.readAll();
			}
			//Close Connections and Readers
			csvReader.close();
			//
			return allContent;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			return null;
		}
	}
	
	public List<String[]> parseFile(String inputFile, int lineNumber)
	{
		try
		{
			
			CSVReader csvReader = new CSVReader(new FileReader(inputFile),',','\"',lineNumber);
			List<String[]> allContent = null;
			String row[] = null;
			row = csvReader.readNext();
			if(row != null)
			{
				allContent = new ArrayList<String[]>();
				allContent = csvReader.readAll();
			}
			//Close Connections and Readers
			csvReader.close();
			//
			return allContent;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			return null;
		}
	}
	
	public List<String[]> parseFile(String inputFile, List<String> tags)
	{
		try
		{
			CSVReader csvReader = new CSVReader(new FileReader(inputFile));
			List<String[]> allContent = null;
			String[] row = null;
			boolean onceBool = base.defaultBoolean();
			while((row = csvReader.readNext())!= null)
			{
				if(onceBool == base.defaultBoolean())
				{
					allContent = new ArrayList<String[]>();
					onceBool = true;
				}
				if(row.length >= 3)
				{
					if(!row[0].equals("Id"))
					{
						if(!row[2].isEmpty())
						{
							String tempRow = htmlParser.removeTags(row[2], tags);
							row[2] = tempRow;
						}
						row[1] = row[1].toLowerCase();
						row[2] = row[2].toLowerCase();
						allContent.add(row);
					}
				}
			}
			
			//Close Connections and Readers
			csvReader.close();
			//
			return allContent;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			return null;
		}
	}
	
}
