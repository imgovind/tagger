package tagPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileCleaner {

	public static base base = new base();
	
	public boolean cleanFile(String inputPath)
	{
		boolean result = base.defaultBoolean();
		if(!inputPath.isEmpty())
		{
			File csvFile = new File(inputPath);
			BufferedReader br = null;
			BufferedWriter bw = null;
			List<String> lines = null;
			lines = new ArrayList<String>();
			try
			{
				String line = base.defaultString();
				br = new BufferedReader(new FileReader(csvFile));
				while((line = br.readLine()) != null )
				{
					if(line.contains("\"\""))
					{
						line = line.replaceAll("\"\"", "'");
					}
					lines.add(line);
				}
				br.close();
				bw = new BufferedWriter(new FileWriter(csvFile));
				bw.write(lines.toString());
				bw.flush();
				bw.close();
				result = true;
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				result = false;
			}
		}
		return result;
	}
	
	public boolean cleanFileAllatOnce(String inputPath)
	{
		boolean result = base.defaultBoolean();
		if(base.IsNotNullOrEmpty(inputPath))
		{
			try
			{
				String fileContent = readFile(inputPath, Charset.defaultCharset());
				fileContent = fileContent.replaceAll("\"\"", "'");
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(inputPath)));
				bw.write(fileContent);
				bw.flush();
				bw.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	private static String readFile(String path, Charset encoding) throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}
}
