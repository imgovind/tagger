package tagPackage;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

public class Main {
	
	public static base base = new base();
	public static RegularEx regex = new RegularEx();
	public static Contractions contraction = new Contractions();
	public static StopWordsAndStem stopStem = new StopWordsAndStem();
	public static VectorSpaceModel vectorSpace = new VectorSpaceModel();
<<<<<<< HEAD
	public static NewParser newParser = new NewParser();
=======
	public static FileCleaner fileCleaner = new FileCleaner();
	
>>>>>>> e6c367c9daf6a86fd7447c6c3930ec8db86b1268
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		String inputTestFile = base.defaultString();
		String inputTrainFile = base.defaultString();
		String outputResultFile = base.defaultString();
		try
		{
			
			//Get The File Input
			if(args.length != 0)
			{
				inputTestFile = args[0];
				inputTrainFile = args[1];
			}
			else
			{
<<<<<<< HEAD
				inputTestFile = "C:\\Users\\gpanneerselvam\\Documents\\Tagger\\Tagger\\files\\Test3.csv";
				inputTrainFile = "C:\\Users\\gpanneerselvam\\Documents\\Tagger\\Tagger\\files\\Train3.csv";
				outputResultFile = "C:\\Users\\gpanneerselvam\\Documents\\Tagger\\Tagger\\files\\result.csv";
=======
				inputTestFile = "/Users/govind/Developer/Tagger/Tagger/files/Test.csv";
				inputTrainFile = "/Users/govind/Developer/Tagger/Tagger/files/Train.csv";
				outputResultFile = "/Users/govind/Developer/Tagger/Tagger/files/Result.csv";
>>>>>>> e6c367c9daf6a86fd7447c6c3930ec8db86b1268
				/*
				 * What about the outputResultFile? What tell! Anybody there?
				base.printElementln("Enter the test file path:");
				inputTestFile = base.getSTRINGinput();
				base.printElementln("Test File Selected");base.printElement(inputTestFile);
				base.printElementln("Enter the train file path:");
				inputTrainFile = base.getSTRINGinput();
				base.printElementln("Train File Selected");base.printElement(inputTrainFile);
				*/
			}
			//
			
			//Parse the file into an array
			FileParse parser = new FileParse();
			ArrayList<String> tags = new ArrayList<String>();
			
			tags.add("pre");
			tags.add("code");
			
<<<<<<< HEAD
			//List<String[]> newTestFile = newParser.parseFileNightly(inputTestFile);
			
			/*
			List<String[]> testFileAll = parser.parseAllFile(inputTestFile);
			List<String[]> trainFileAll = parser.parseAllFile(inputTrainFile);
			*/
			
=======
			//boolean fileTransformed = fileCleaner.cleanFileAllatOnce(inputTestFile);
			//if(fileTransformed == false)
			//{
			//	boolean fileTransformed2 = fileCleaner.cleanFile(inputTestFile);
			//}
			base.printElementln("File Parsing Started.....");
>>>>>>> e6c367c9daf6a86fd7447c6c3930ec8db86b1268
			List<String[]> testFile = parser.parseFile(inputTestFile, tags);
			List<String[]> trainFile = parser.parseFile(inputTrainFile, tags);
			
			base.printElementln("Removing Special Characters.....");
			List<String[]> test = regex.RemoveSpecialCharacters(testFile);
			List<String[]> train = regex.RemoveSpecialCharacters(trainFile);
			
<<<<<<< HEAD
			
			List<String[]> test = regex.RemoveSpecialCharacters(testFile);
			List<String[]> train = regex.RemoveSpecialCharacters(trainFile);
			
=======
			base.printElementln("Removing Language Contractions.....");
>>>>>>> e6c367c9daf6a86fd7447c6c3930ec8db86b1268
			test = contraction.RemoveContractions(test);
			train = contraction.RemoveContractions(train);
			
			base.printElementln("Getting tf Words for Test File's Title");
			HashMap<String,String> testTitleBag = stopStem.titleBag(test);
			base.printElementln("Getting tf Words for Test File's Body");
			HashMap<String,String> testBodyBag = stopStem.bodyBag(test);
			base.printElementln("Getting tf Words for Train File's Title");
			HashMap<String,String> trainTitleBag = stopStem.titleBag(train);
			base.printElementln("Getting tf Words for Train File's Body");
			HashMap<String,String> trainBodyBag = stopStem.bodyBag(train);
			
			base.printElementln("Getting BOW for Test File's Title");
			HashMap<String,HashMap<String,Integer>> testTitleBOW = vectorSpace.getBOW(testTitleBag);
			base.printElementln("Getting BOW for Test File's Body");
			HashMap<String,HashMap<String,Integer>> testBodyBOW = vectorSpace.getBOW(testBodyBag);
			base.printElementln("Getting BOW for Train File's Title");
			HashMap<String,HashMap<String,Integer>> trainTitleBOW = vectorSpace.getBOW(trainTitleBag);
			base.printElementln("Getting BOW for Train File's Body");
			HashMap<String,HashMap<String,Integer>> trainBodyBOW = vectorSpace.getBOW(trainBodyBag);
			
			base.printElementln("Getting Final Mapped Tags for Test File's Title");
			HashMap<String,String> titleTagMap = vectorSpace.compareBOW(testTitleBOW, trainTitleBOW, vectorSpace.getTrainTags(train), 2);
			base.printElementln("Getting Final Mapped Tags for Test File's Body");
			HashMap<String,String> bodyTagMap = vectorSpace.compareBOW(testBodyBOW, trainBodyBOW, vectorSpace.getTrainTags(train), 1);
			
<<<<<<< HEAD
=======
			base.printElementln("Combining Title Tags and Body Tags");
>>>>>>> e6c367c9daf6a86fd7447c6c3930ec8db86b1268
			List<String[]> result = combineTitleAndBody(titleTagMap,bodyTagMap,testFile);
			
			base.printElementln("Writing Content to File");
			CSVWriter writer = new CSVWriter(new FileWriter(outputResultFile),',');
			writer.writeAll(result);
			writer.close();
			
			base.printElementln("File Written to the Following Path => ");
			base.printElement(outputResultFile);
			//This is where I write the file, there is already a breakpoint, check if you get any exception
			/*//Can you make eclipse not full screen
			 * 
			String outputWawa = stopStem.removeStopWords("I am a very very good girl who is also trying to remove stop words");
			base.printElement("INPUT:");;base.printElementln("I am a very very good girl who is also trying to remove stop words");
			base.printElement("First:");System.out.println(outputWawa);base.printElementln("");
			outputWawa = stopStem.ProcessStopWordsAndStem("I am a very very good girl who is also trying to remove stop words");
			base.printElement("Second");System.out.println(outputWawa);base.printElementln("");
			*/
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	public static List<String[]> combineTitleAndBody(HashMap<String,String> titleTagMap, HashMap<String,String> bodyTagMap, List<String[]> testFile)
	{
		List<String[]> result = new ArrayList<String[]>();
		List<String> resultList = null;
		for(String[] row: testFile)
		{
			resultList = new ArrayList<String>();
			String QuestionID = row[0];
			String QuestionTitle = row[1];
			String QuestionBody = row[2];
			String QuestionTags = titleTagMap.get(QuestionID) + " " + bodyTagMap.get(QuestionID);
			resultList.add(QuestionID);
			resultList.add(QuestionTitle);
			resultList.add(QuestionBody);
			resultList.add(QuestionTags);
			result.add(resultList.toArray(new String[resultList.size()]));
		}
		return result;
	}

}
