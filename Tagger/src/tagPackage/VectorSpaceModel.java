package tagPackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

public class VectorSpaceModel {
	public static base base = new base();
	public HashMap<String,String> getTrainTags(List<String[]> inputList)
	{
		HashMap<String,String> result = null;
		
		if(inputList != null && !inputList.isEmpty())
		{
			result = new HashMap<String,String>();
			for(String[] row: inputList)
			{
				if(row.length == 4)
				{
					result.put(row[0], row[3]);
				}
			}
		}
		return result;
	}
	
	public HashMap<String,HashMap<String,Integer>> getBOW(HashMap<String,String> inputMap)
	{
		HashMap<String,HashMap<String,Integer>> result = null;
		if(inputMap != null && !inputMap.isEmpty())
		{
			result = new HashMap<String,HashMap<String,Integer>>();
			HashSet<String> BagOfWords = null;
			HashMap<String,Integer> WordAndFrequency = null;
			Iterator<Entry<String,String>> iter = inputMap.entrySet().iterator();
			while(iter.hasNext())
			{
				Entry<String,String> kvp = (Entry<String,String>)iter.next();
				if(kvp != null && !kvp.getValue().isEmpty())
				{
					BagOfWords = new HashSet<String>();
					WordAndFrequency = new HashMap<String,Integer>();
					String[] Bag = kvp.getValue().split(" ");
					if(Bag != null)
					{
						for(String wordInBag: Bag)
						{
							if(!BagOfWords.contains(wordInBag))
							{
								BagOfWords.add(wordInBag);
								WordAndFrequency.put(wordInBag, 1);
							}
							else
							{
								int frequency = WordAndFrequency.get(wordInBag);
								frequency = frequency + 1;
								WordAndFrequency.remove(wordInBag);
								WordAndFrequency.put(wordInBag, frequency);
							}
						}
					}
				}
				result.put(kvp.getKey(), WordAndFrequency);
			}
		}
		return result;
	}
	
	public HashMap<String,String> compareBOW(HashMap<String,HashMap<String,Integer>> testBOW, HashMap<String,HashMap<String,Integer>> trainBOW, HashMap<String,String> trainTags, int topKValues)
	{
		HashMap<String,String> result = null;
		
		HashMap<String,String[]> cosineResult = this.calculateCosine(testBOW, trainBOW, trainTags); 
		result = new HashMap<String,String>();
		Iterator<Entry<String,String[]>> iter = cosineResult.entrySet().iterator();
		while(iter.hasNext())
		{
			Entry<String,String[]> kvp = iter.next();
			if(kvp != null && !kvp.getValue().toString().isEmpty())
			{
				String testQuestionID = kvp.getKey();
				String[] trainInformation = kvp.getValue();
				if(trainInformation.length > topKValues * 4)
				{
					TreeMap<Integer,String> idWeights = new TreeMap<Integer,String>();
					TreeMap<Double,String> idCosines = new TreeMap<Double,String>();
					HashMap<String,String> idTags = new HashMap<String,String>();
					for(int i=0; i< trainInformation.length; i=i+4)
					{	
						idWeights.put(Integer.parseInt(trainInformation[i+1]),trainInformation[i]);
						idCosines.put(Double.parseDouble(trainInformation[i+2]),trainInformation[i]);
						idTags.put(trainInformation[i], trainInformation[i+3]);
					}
					int size = idWeights.size();
					int sizeminus1 = size - 1;
					int i = base.defaultInt();
					Entry<Integer,String> maxEntry = new MyEntry<Integer, String>(base.defaultInt(),base.defaultString());
					Entry<Integer,String> secondMaxEntry = new MyEntry<Integer, String>(base.defaultInt(),base.defaultString());
					for(Entry<Integer,String> mapEntry: idWeights.entrySet())
					{
						if(i == sizeminus1)
						{
							secondMaxEntry = mapEntry;
						}
						if(i == size)
						{
							maxEntry = mapEntry;
						}
					}
					String testTags = base.defaultString();
					if(maxEntry.getValue() == secondMaxEntry.getValue())
					{
						size = idCosines.size();
						sizeminus1 = size - 1;
						i = base.defaultInt();
						Entry<Double,String> CmaxEntry = new MyEntry<Double, String>(base.defaultDouble(),base.defaultString());
						Entry<Double,String> CsecondMaxEntry = new MyEntry<Double, String>(base.defaultDouble(),base.defaultString());
						for(Entry<Double,String> mapEntry: idCosines.entrySet())
						{
							if(i == sizeminus1)
							{
								CsecondMaxEntry = mapEntry;
							}
							if(i == size)
							{
								CmaxEntry = mapEntry;
							}
						}
						testTags += idTags.get(CmaxEntry.getKey());
						if(topKValues > 1)
						{
							testTags += idTags.get(CsecondMaxEntry.getKey());
						}
					}
					else
					{
						testTags += idTags.get(maxEntry.getKey());
						if(topKValues > 1)
						{
							testTags += idTags.get(secondMaxEntry.getKey());
						}
					}
					result.put(testQuestionID, testTags);
				}
				else
				{
					String testTags = base.defaultString();
					for(int i=0; i< trainInformation.length; i=i+4)
					{	
						testTags += trainInformation[i+3];
						testTags += " ";
					}
					result.put(testQuestionID, testTags);
				}
			}
		}	
		return result;
	}
	
	private HashMap<String,String[]> calculateCosine(HashMap<String,HashMap<String,Integer>> testBOW, HashMap<String,HashMap<String,Integer>> trainBOW, HashMap<String,String> trainTags)
	{
		HashMap<String,String[]> result = new HashMap<String,String[]>();
		if(testBOW != null && trainBOW != null && !testBOW.isEmpty() && !trainBOW.isEmpty())
		{
			Iterator<Entry<String, HashMap<String, Integer>>> testIter = testBOW.entrySet().iterator();
			while(testIter.hasNext())
			{
				Entry<String, HashMap<String,Integer>> testKVP = testIter.next();
				if(testKVP != null && testKVP.getValue() != null && !testKVP.getValue().isEmpty())
				{
					//One Iteration of TestBOW
					String testQuestionID = testKVP.getKey();
					HashMap<String,Integer> testWFMap = testKVP.getValue();
					//Result Writer Declarer
					List<String> testValueList = new ArrayList<String>();
					//GettingTrain
					Iterator<Entry<String, HashMap<String, Integer>>> trainIter = trainBOW.entrySet().iterator();
					while(trainIter.hasNext())
					{
						Entry<String, HashMap<String,Integer>> trainKVP = trainIter.next();
						if(trainKVP != null && !trainKVP.getValue().isEmpty())
						{
							String trainQuestionID = trainKVP.getKey();
							HashMap<String,Integer> trainWFMap = trainKVP.getValue();
							HashSet<String> testKeys = new HashSet<String>(testWFMap.keySet());
							HashSet<String> trainKeys = new HashSet<String>(trainWFMap.keySet());
							trainKeys.retainAll(testKeys);
							if(trainKeys != null && !trainKeys.isEmpty())
							{
								int frequencyMultiplier = base.defaultInt();
								for(String wordInTrainBag: trainKeys)
								{
									int testFrequency = base.defaultInt();
									int trainFrequency = base.defaultInt();
									if(testWFMap.containsKey(wordInTrainBag))
									{
										testFrequency =  testWFMap.get(wordInTrainBag);
									}
									if(trainWFMap.containsKey(wordInTrainBag))
									{
										trainFrequency = trainWFMap.get(wordInTrainBag);
									}
									int multiplier = testFrequency * trainFrequency;
									frequencyMultiplier += multiplier;
								}
								double frequencyCosine = Math.cos((double)frequencyMultiplier);
								testValueList.add(trainQuestionID);
								testValueList.add(Integer.toString(trainKeys.size()));
								testValueList.add(Double.toString(frequencyCosine));
								testValueList.add(trainTags.get(trainQuestionID));
							}
						}
					}
					//
					if(testValueList.size() > 0)
					{
						String[] testValueListArray = testValueList.toArray(new String[testValueList.size()]);
						result.put(testQuestionID, testValueListArray);
					}
				}
			}
		}
		return result;
	}
}
