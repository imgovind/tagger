package tagPackage;

import java.util.Scanner;

public class base {
	private static final String EMPTY_STRING = "";
	private static int EMPTY_INT;
	private static float EMPTY_FLOAT;
	private static double EMPTY_DOUBLE;
	
	public Scanner reader = new Scanner(System.in);
	
	public boolean defaultBoolean()
	{
		return false;
	}
	
	public boolean IsNotNullOrEmpty(Object o)
	{
		boolean result = this.defaultBoolean();
		String temp = o.toString();
		if(temp != null)
		{
			if(!temp.isEmpty())
			{
				result = true;
			}
		}
		return result;
	}
	
	public String defaultString()
	{
		return EMPTY_STRING;
	}
	
	public int defaultInt()
	{
		return EMPTY_INT;
	}
	
	public double defaultDouble()
	{
		return EMPTY_DOUBLE;
	}
	
	public void printElementln(Object input)
	{
		String outputResult = input.toString();
		System.out.println(outputResult);
	}
	
	public void printElement(Object input)
	{
		String outputResult = input.toString();
		System.out.print(outputResult);
	}
	
	public double getDOUBLEinput()
	{
		double result = EMPTY_FLOAT;
		
		if(reader.hasNextDouble())
		{
			result = reader.nextDouble();
		}
		
		return result;
	}
	
	public float getFLOATinput()
	{
		float result = EMPTY_FLOAT;
		
		if(reader.hasNextFloat())
		{
			result = reader.nextFloat();
		}
		
		return result;
	}
	
	public int getINTEGERinput()
	{
		int result = EMPTY_INT;
		
		if(reader.hasNextInt())
		{
			result = reader.nextInt();
		}
		
		return result;
	}
	
	public String getSTRINGinput()
	{
		String result = EMPTY_STRING;
		
		if(reader.hasNextLine())
		{
			result = reader.nextLine();
		}
		
		return result;
	}
	
	public void closeReader()
	{
		reader.close();
	}
}
